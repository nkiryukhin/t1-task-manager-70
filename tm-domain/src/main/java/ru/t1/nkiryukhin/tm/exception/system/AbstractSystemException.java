package ru.t1.nkiryukhin.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

public abstract class AbstractSystemException extends AbstractException {

    public AbstractSystemException(@NotNull String message) {
        super(message);
    }

}
