package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.nkiryukhin.tm.api.endpoint.IAdminEndpoint;
import ru.t1.nkiryukhin.tm.api.service.IDomainService;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.dto.request.DatabaseCreateRequest;
import ru.t1.nkiryukhin.tm.dto.request.DatabaseDropRequest;
import ru.t1.nkiryukhin.tm.dto.response.DatabaseCreateResponse;
import ru.t1.nkiryukhin.tm.dto.response.DatabaseDropResponse;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.nkiryukhin.tm.api.endpoint.IAdminEndpoint")
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @WebMethod
    public DatabaseDropResponse dropDatabase(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DatabaseDropRequest request
    ) throws AccessDeniedException {
        String passphrase = request.getPassphrase();
        if (passphrase == null || !passphrase.equals(propertyService.getDbaPassphrase()))
            throw new AccessDeniedException();
        domainService.dropDatabase();
        return new DatabaseDropResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DatabaseCreateResponse createDatabase(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DatabaseCreateRequest request
    ) {
        domainService.createDatabase();
        return new DatabaseCreateResponse();
    }

}