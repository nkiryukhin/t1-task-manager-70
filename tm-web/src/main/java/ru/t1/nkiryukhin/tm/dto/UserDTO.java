package ru.t1.nkiryukhin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "web_user")
public final class UserDTO {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String login;

    @Column(name = "password_hash")
    private String passwordHash;

}
