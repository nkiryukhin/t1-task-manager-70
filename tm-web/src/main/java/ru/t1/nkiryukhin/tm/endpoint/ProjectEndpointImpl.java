package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.nkiryukhin.tm.api.endpoint.ProjectEndpoint;
import ru.t1.nkiryukhin.tm.dto.ProjectDTO;
import ru.t1.nkiryukhin.tm.repository.ProjectDTORepository;
import ru.t1.nkiryukhin.tm.repository.ProjectRepository;
import ru.t1.nkiryukhin.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.nio.file.AccessDeniedException;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.nkiryukhin.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectDTORepository projectDTORepository;

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() throws AccessDeniedException {
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() throws AccessDeniedException {
        return projectDTORepository.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(@RequestBody @NotNull List<ProjectDTO> projects) throws AccessDeniedException {
        for (ProjectDTO project : projects) {
            projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project.getId());
        }
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) throws AccessDeniedException {
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull final ProjectDTO project) throws AccessDeniedException {
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project.getId());
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) throws AccessDeniedException {
        return projectDTORepository.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<ProjectDTO> findAll() throws AccessDeniedException {
        return projectDTORepository.findAllByUserId(UserUtil.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(@PathVariable("id") final @NotNull String id) throws AccessDeniedException {
        return projectDTORepository.findByUserIdAndId(UserUtil.getUserId(), id).orElse(null);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(@RequestBody @NotNull final ProjectDTO project) throws AccessDeniedException {
        project.setUserId(UserUtil.getUserId());
        projectDTORepository.save(project);
    }

}