package ru.t1.nkiryukhin.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseName();

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseServerUrl();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHbm2ddlAuto();

    @NotNull
    String getDatabaseShowSQL();

    @NotNull
    String getDatabaseUseSecondLevelCache();

    @NotNull
    String getDatabaseUseQueryCache();

    @NotNull
    String getDatabaseUseMinimalPuts();

    @NotNull
    String getDatabaseCacheRegionPrefix();

    @NotNull
    String getDatabaseCacheProviderConfigurationFile();

    @NotNull
    String getDatabaseCacheRegionFactoryClass();

}