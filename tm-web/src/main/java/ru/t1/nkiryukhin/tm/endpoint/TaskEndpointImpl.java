package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.nkiryukhin.tm.api.endpoint.TaskEndpoint;
import ru.t1.nkiryukhin.tm.dto.TaskDTO;
import ru.t1.nkiryukhin.tm.repository.TaskDTORepository;
import ru.t1.nkiryukhin.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.nio.file.AccessDeniedException;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.nkiryukhin.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private TaskDTORepository taskRepository;

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() throws AccessDeniedException {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() throws AccessDeniedException {
        return taskRepository.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) throws AccessDeniedException {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull final TaskDTO task) throws AccessDeniedException {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task.getId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(@RequestBody @NotNull List<TaskDTO> tasks) throws AccessDeniedException {
        for (TaskDTO task : tasks) {
            taskRepository.deleteByUserIdAndId(UserUtil.getUserId(),task.getId());
        }
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) throws AccessDeniedException {
        return taskRepository.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<TaskDTO> findAll() throws AccessDeniedException {
        return taskRepository.findAllByUserId(UserUtil.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDTO findById(@PathVariable("id") @NotNull final String id) throws AccessDeniedException {
        return taskRepository.findByUserIdAndId(UserUtil.getUserId(), id).orElse(null);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(@RequestBody @NotNull final TaskDTO task) throws AccessDeniedException {
        task.setUserId(UserUtil.getUserId());
        taskRepository.save(task);
    }

}