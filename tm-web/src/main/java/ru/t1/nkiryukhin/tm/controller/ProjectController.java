package ru.t1.nkiryukhin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.model.CustomUser;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.model.User;
import ru.t1.nkiryukhin.tm.repository.ProjectRepository;
import ru.t1.nkiryukhin.tm.repository.UserRepository;

@Controller
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/project/create")
    public String create(
            @AuthenticationPrincipal final CustomUser customUser
    ) {
        final Project project = new Project("New Project " + System.currentTimeMillis());
        User user = userRepository.findById(customUser.getUserId()).orElse(null);
        project.setUser(user);
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @Transactional
    @GetMapping("/project/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser customUser,
            @PathVariable("id") String id
    ) {
        projectRepository.deleteByUserIdAndId(customUser.getUserId(), id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser customUser,
            @ModelAttribute("project") Project project,
            BindingResult result
    ) {
        User user = userRepository.findById(customUser.getUserId()).orElse(null);
        project.setUser(user);
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser customUser,
            @PathVariable("id") String id
    ) {
        final Project project = projectRepository.findByUserIdAndId(customUser.getUserId(), id).orElse(null);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}