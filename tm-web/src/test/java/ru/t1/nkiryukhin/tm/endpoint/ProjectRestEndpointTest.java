package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.client.ProjectRestEndpointClient;
import ru.t1.nkiryukhin.tm.marker.IntegrationCategory;
import ru.t1.nkiryukhin.tm.model.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Category(IntegrationCategory.class)
public final class ProjectRestEndpointTest {

    @NotNull
    final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @NotNull
    final Project project1 = new Project("Test Project 1");

    @NotNull
    final Project project2 = new Project("Test Project 2");

    @Test
    public void clear() {
        Collection<Project> backupProjects = client.findAll();
        client.clear();
        Collection<Project> projects = client.findAll();
        Assert.assertTrue(projects.size() == 0);
        for (Project project : backupProjects) {
            client.save(project);
        }
    }

    @Test
    public void countDeleteProjects() {
        long initCount = client.count();
        client.save(project1);
        long endCount = client.count();
        Assert.assertEquals(1, endCount - initCount);
        client.delete(project1);
        Assert.assertNull(client.findById(project1.getId()));
    }

    @Test
    public void findAllDeleteAllProjects() {
        client.save(project1);
        client.save(project2);
        Collection<Project> projects = client.findAll();
        int i = 0;
        for (Project project : projects) {
            if (project.equals(project1) || project.equals(project2)) i++;
        }
        Assert.assertEquals(i, 2);
        List<Project> addedProjects = new ArrayList<>();
        addedProjects.add(project1);
        addedProjects.add(project2);
        client.deleteList(addedProjects);
    }

    @Test
    public void existsByIdProject() {
        @NotNull final Project project = new Project("Test Project");
        client.save(project);
        Assert.assertTrue(client.existsById(project.getId()));
        client.deleteById(project.getId());
        Assert.assertFalse(client.existsById(project.getId()));
    }

    @Test
    public void saveDeleteFindProject() {
        @NotNull final Project project = new Project("Test Project");
        client.save(project);
        Project foundProject = client.findById(project.getId());
        Assert.assertNotNull(foundProject);
        Assert.assertEquals(project.getName(), foundProject.getName());
        client.deleteById(project.getId());
        Assert.assertNull(client.findById(project.getId()));
    }

}