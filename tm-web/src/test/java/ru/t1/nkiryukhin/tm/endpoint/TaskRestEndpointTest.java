package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.client.TaskRestEndpointClient;
import ru.t1.nkiryukhin.tm.marker.IntegrationCategory;
import ru.t1.nkiryukhin.tm.model.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(IntegrationCategory.class)
public final class TaskRestEndpointTest {

    @NotNull
    final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @NotNull
    final Task task1 = new Task("Test Task 1");

    @NotNull
    final Task task2 = new Task("Test Task 2");

    @Test
    public void clear() {
        Collection<Task> backupTasks = client.findAll();
        client.clear();
        Collection<Task> tasks = client.findAll();
        Assert.assertEquals(0, tasks.size());
        for (Task task : backupTasks) {
            client.save(task);
        }
    }

    @Test
    public void countTasksAndDeleteTasks() {
        long initCount = client.count();
        client.save(task1);
        long endCount = client.count();
        Assert.assertEquals(1, endCount - initCount);
        client.delete(task1);
        Assert.assertNull(client.findById(task1.getId()));
    }

    @Test
    public void findAllTasksAndDeleteAllTasks() {
        client.save(task1);
        client.save(task2);
        Collection<Task> tasks = client.findAll();
        int i = 0;
        for (Task task : tasks) {
            if (task.equals(task1) || task.equals(task2)) i++;
        }
        Assert.assertEquals(i, 2);
        List<Task> addedTasks = new ArrayList<>();
        addedTasks.add(task1);
        addedTasks.add(task2);
        client.clear(addedTasks);
    }

    @Test
    public void existsByIdTask() {
        @NotNull final Task task = new Task("Test Task");
        client.save(task);
        Assert.assertTrue(client.existsById(task.getId()));
        client.deleteById(task.getId());
        Assert.assertFalse(client.existsById(task.getId()));
    }

    @Test
    public void saveTaskAndDeleteTaskAndFindTask() {
        @NotNull final Task task = new Task("Test Task");
        client.save(task);
        Task foundTask = client.findById(task.getId());
        Assert.assertNotNull(foundTask);
        Assert.assertEquals(task.getName(), foundTask.getName());
        client.deleteById(task.getId());
        Assert.assertNull(client.findById(task.getId()));
    }

}